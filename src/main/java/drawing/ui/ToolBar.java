package drawing.ui;

import drawing.ButtonFactory;
import drawing.commands.CommandHistory;
import drawing.handlers.*;
import drawing.handlers.EllipseButtonHandler;
import drawing.handlers.RectangleButtonHandler;
import drawing.handlers.TriangleButtonHandler;
import drawing.shapes.IShape;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.List;


public class ToolBar {
    private Button clearButton;

    private Button deleteButton;
    private Button rectangleButton;
    private Button circleButton;
    private Button triangleButton;

    private Button groupeButton;

    private Button degroupeButton;

    private Button annulerButton;

    private Button redoButton;
    public static int TEXT_ONLY = 0;
    public static int ICON_ONLY = 1;

    private CommandHistory commandHistory;
    private List<IShape> selectedShapes;
    private DrawingPane drawingPane;


    public ToolBar(CommandHistory commandHistory, List<IShape> selectedShapes, DrawingPane drawingPane){

        this.commandHistory = commandHistory;
        this.selectedShapes = selectedShapes;
        this.drawingPane = drawingPane;

        ButtonFactory boutton = new ButtonFactory(TEXT_ONLY);
        clearButton = boutton.createButton("clearButton");
        clearButton.addEventFilter(ActionEvent.ACTION, new ClearButtonHandler(drawingPane));

        deleteButton = boutton.createButton("Delete");
        deleteButton.addEventFilter(ActionEvent.ACTION, new DeleteSelectedButtonHandler(drawingPane));

        rectangleButton = boutton.createButton("Rectangle");
        rectangleButton.addEventFilter(ActionEvent.ACTION, new RectangleButtonHandler(drawingPane));

        circleButton = boutton.createButton("Circle");
        circleButton.addEventFilter(ActionEvent.ACTION, new EllipseButtonHandler(drawingPane));

        triangleButton = boutton.createButton("Triangle");
        triangleButton.addEventFilter(ActionEvent.ACTION, new TriangleButtonHandler(drawingPane));

        groupeButton = boutton.createButton("Grouper");
        groupeButton.setOnMouseClicked(new GroupeShapesSelectedHandler(commandHistory, selectedShapes, drawingPane));

        degroupeButton = boutton.createButton("Degrouper");
        degroupeButton.addEventFilter(ActionEvent.ACTION, new DegroupeShapesSelected(drawingPane));

        annulerButton = boutton.createButton("Annuler");
        annulerButton.addEventFilter(ActionEvent.ACTION, new UndoButtonHandler(drawingPane));

        redoButton = boutton.createButton("Redo");
        redoButton.addEventFilter(ActionEvent.ACTION, new RedoButtonHandler(drawingPane));

    }



    public Button getClearButton(){
        return clearButton;
    }

    public Button getDeleteButton(){
        return deleteButton;
    }

    public Button getRectangleButton(){
        return rectangleButton;
    }

    public Button getCircleButton(){
        return circleButton;
    }

    public Button getTriangleButton(){
        return triangleButton;
    }

    public Button getGroupeButton(){
        return groupeButton;
    }

    public Button getAnnulerButton(){
        return annulerButton;
    }

    public Button getDegroupeButton(){ return degroupeButton; }
    public Button getRedoButton(){ return redoButton; }
}
