package drawing.ui;

import drawing.Observer;
import drawing.handlers.DrawingPane;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;

public class StatusBars extends HBox implements Observer {

    Label labels;
    Label selected_shapes;
    DrawingPane drawingPane;

    public StatusBars(DrawingPane drawingPane){

        this.drawingPane = drawingPane;
        drawingPane.attach(this); // notre classe est un observer

        labels = new Label("formes");
        labels.setPrefWidth(110);

        selected_shapes = new Label("Selected Labels");
       // selected_shapes.setPrefWidth();

        this.setPadding(new Insets(5));
        this.setSpacing(5.0);
        this.getStyleClass().add("toolbar");
        this.getChildren().add(labels);
        this.getChildren().add(selected_shapes);
    }

    @Override
    public void update() {
       // System.out.println(getIteratorSize(drawingPane.iterator()));
        this.labels.setText("forms : "+drawingPane.getIteratorSize(drawingPane.iterator()));
        this.selected_shapes.setText("selected shapes : "+drawingPane.getIteratorSize(drawingPane.getSelection()));
    }


}
