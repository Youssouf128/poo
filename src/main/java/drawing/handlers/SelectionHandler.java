package drawing.handlers;

import drawing.shapes.IShape;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;
import java.util.Iterator;

public class SelectionHandler implements EventHandler<MouseEvent>, Iterable<IShape> {
    private DrawingPane drawingPane;
    private ArrayList<IShape> selectedShapes;

    public SelectionHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        this.drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        this.selectedShapes = new ArrayList<>();
    }
    @Override
    public void handle(MouseEvent event) {
        IShape isOnShape = null;
        for (IShape s: drawingPane)
            if (s.isOn(event.getX(), event.getY())) {
                isOnShape = s;
                break;
            }

        if (isOnShape != null) {
            if (selectedShapes.contains(isOnShape)) {
                if (event.isShiftDown() && selectedShapes.size() > 1)
                    removeFromSelection(isOnShape);

            } else {
                if (!event.isShiftDown()) {
                    clearSelection();
                }
                addToSelection(isOnShape);
            }
        } else {
            clearSelection();
        }

        // affiche liste shapes
        System.out.println(selectedShapes);
    }

    private void addToSelection(IShape shape) {
        if (selectedShapes.contains(shape))
            return;
        shape.setSelected(true);
        selectedShapes.add(shape);
        drawingPane.notifyAllObservers();
    }

    private void removeFromSelection(IShape shape) {
        if (!selectedShapes.contains(shape))
            return;
        shape.setSelected(false); // on le deselection
        selectedShapes.remove(shape); // on le supprimer de notre liste
        drawingPane.notifyAllObservers();
    }

    private void clearSelection() {
        selectedShapes.forEach(iShape -> iShape.setSelected(false));
        selectedShapes.clear();
        drawingPane.notifyAllObservers();
    }

    @Override
    public Iterator<IShape> iterator() {
        return selectedShapes.iterator();
    }

}
