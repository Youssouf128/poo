package drawing.handlers;

import drawing.commands.ClearCommand;
import drawing.commands.DeleteSelectedCommand;
import drawing.commands.ICommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DeleteSelectedButtonHandler implements EventHandler<ActionEvent> {


    private DrawingPane drawingPane;
    private ICommand delete_cmd;
    public DeleteSelectedButtonHandler(DrawingPane drawingPane) {

        this.drawingPane = drawingPane;
        delete_cmd = new DeleteSelectedCommand(drawingPane);
    }

    @Override
    public void handle(ActionEvent event) {

        drawingPane.getCommandHistory().exec(delete_cmd);
    }

}
