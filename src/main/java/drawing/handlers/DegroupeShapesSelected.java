
package drawing.handlers;

        import drawing.shapes.IShape;
        import drawing.shapes.ShapeComposite;
        import javafx.event.ActionEvent;
        import javafx.event.EventHandler;

        import java.awt.*;
        import java.util.Iterator;

public class DegroupeShapesSelected implements EventHandler<ActionEvent> {

    private DrawingPane drawingPane;

    public DegroupeShapesSelected(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {

        //drawingPane

        Iterator iterator = drawingPane.getSelection();

        while (iterator.hasNext()) {
            IShape shape = (IShape) iterator.next();

            if (shape instanceof ShapeComposite){

               ShapeComposite shape_a = (ShapeComposite) shape;

              drawingPane.removeShape(shape);

                Iterator iter = shape_a.iterator();
                while (iter.hasNext()) {
                    IShape shp = (IShape) iter.next();
                    drawingPane.addShape(shp);
                }

            }
        }

    }

}

