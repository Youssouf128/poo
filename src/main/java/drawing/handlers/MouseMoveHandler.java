package drawing.handlers;

import drawing.shapes.IShape;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.Iterator;

/**
 * Created by lewandowski on 20/12/2020.
 */
public class MouseMoveHandler implements EventHandler<MouseEvent>{

    private DrawingPane drawingPane;

    private double orgSceneX;
    private double orgSceneY;
    private double orgTranslateX;
    private double orgTranslateY;

    private IShape selectedShape;

    public MouseMoveHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        drawingPane.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        drawingPane.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
    }

    @Override
    public void handle(MouseEvent event) {

        if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
            orgSceneX = event.getSceneX(); // CLICK MOUSE PRESSED X
            orgSceneY = event.getSceneY(); // CLICK MOUSE PRESSED Y

            /*
            Iterator iterator = drawingPane.iterator();

            while(iterator.hasNext()){
                IShape shape = (IShape) iterator.next();

                if (shape.isOn(event.getX(), event.getY())){
                    selectedShape = shape;
                    System.out.println(event.getX()+" " + " "+event.getY());
                    break;
                }
            }
            */

            //orgTranslateX = selectedShape == null ? 0 : selectedShape.getTranslateX();
            //orgTranslateY = selectedShape == null ? 0 : selectedShape.getTranslateY();
            /*
            if(selectedShape != null){
                selectedShape.setSelected(true);
            }*/

        }

        if (event.getEventType().equals(MouseEvent.MOUSE_DRAGGED)) {
            //System.out.println("kkk");
            if (getIteratorSize(drawingPane.getSelection()) == 0)
                return;


            double offsetX = event.getSceneX() - orgSceneX;
            double offsetY = event.getSceneY() - orgSceneY;

            Iterator iterator = drawingPane.iterator();

            while(iterator.hasNext()) {
                IShape shape = (IShape) iterator.next();

                shape.offset(offsetX, offsetY);// deplacer
            }
        }

        if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {


        }
    }

    public int getIteratorSize(Iterator iterator){

        int count = 0;
        while(iterator.hasNext()){
            iterator.next();
            count++;
        }
        return count;
    }

    public DrawingPane getDrawingPane(){
        return drawingPane;
    }
}
