package drawing.handlers;

import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.shapes.IShape;
import drawing.Observer;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

import java.util.List;

/**
 * Created by lewandowski on 20/12/2020.
 */
public class DrawingPane extends Pane implements Iterable<IShape> {

    private MouseMoveHandler mouseMoveHandler;

    private SelectionHandler selectionHandler;

    private List<Observer> observers = new ArrayList<Observer>();

    private List<IShape> shapes;

    public CommandHistory command_history;

    int current = 0;

    public DrawingPane() {
        clipChildren();
        shapes = new ArrayList<>();
        mouseMoveHandler = new MouseMoveHandler(this);
        selectionHandler = new SelectionHandler(this);
        command_history = new CommandHistory();
    }

    public Iterator<IShape> getSelection(){
        return selectionHandler.iterator();
    }

    public CommandHistory getCommandHistory(){
        return command_history;
    }

    public void setCommandHistory(CommandHistory command){
        this.command_history = command;
    }
    // cette méthode va stocker tous les observers pour nous ça sera la class StatusBars
    public void attach(Observer observer){
        observers.add(observer);
    }

    // cette méthode permet de supprimer un observer dans une liste
    public void delete(Observer observer){
        observers.remove(observer);
    }

    /**
     * Clips the children of this {@link Region} to its current size.
     * This requires attaching a change listener to the region’s layout bounds,
     * as JavaFX does not currently provide any built-in way to clip children.
     */
    void clipChildren() {
        final Rectangle outputClip = new Rectangle();
        this.setClip(outputClip);

        this.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            outputClip.setWidth(newValue.getWidth());
            outputClip.setHeight(newValue.getHeight());
        });
    }

    // cette méthode permet l'ajoute de chaque forme crée dans une liste de shape et de l'affiche en écran
    // nous en profitons pour notifier a tous les observer qu'un element a été ajouté et nous le metons a jour.
    public void addShape(IShape shape) {

        shapes.add(shape);
        shape.addShapeToPane(this);
        notifyAllObservers();
    }

    public void removeShape(IShape shape) {
        shapes.remove(shape);
        shape.removeShapeFromPane(this);
        notifyAllObservers();
    }


    public void clear() {

        for (IShape shape : shapes){
            shape.removeShapeFromPane(this);
        }
        shapes.clear();
        notifyAllObservers();
        deleteAllShapsSelected();
    }

    public void deleteAllShapsSelected(){
        if(getIteratorSize(getSelection()) > 0){

            Iterator iterator = getSelection();
            while (iterator.hasNext()) {
                IShape shape = (IShape) iterator.next();
                removeShape(shape);
                iterator.remove(); // solution
            }
            notifyAllObservers();
        }
    }

    // cette methode notifie a tous les observer qu'il y'a eu un changement
    void notifyAllObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    @Override
    public Iterator<IShape> iterator() {
        return shapes.iterator();
    }

    public int getIteratorSize(Iterator iterator){

        int count = 0;
        while(iterator.hasNext()){
            iterator.next();
            count++;
        }
        return count;
    }
}
