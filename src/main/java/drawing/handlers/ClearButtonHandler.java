package drawing.handlers;

import drawing.commands.ClearCommand;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ClearButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;

    public ClearButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        ICommand clearCommand = new ClearCommand(drawingPane);
        commandHistory.exec(clearCommand);
    }
}
