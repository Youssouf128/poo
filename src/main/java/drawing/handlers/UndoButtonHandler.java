package drawing.handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class UndoButtonHandler implements EventHandler<ActionEvent> {

    DrawingPane drawingPane;

    public UndoButtonHandler(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent actionEvent) {

        this.drawingPane.getCommandHistory().undo();
    }
}
