package drawing.handlers;

import java.util.ArrayList;
import java.util.List;

import drawing.shapes.IShape;
import javafx.scene.layout.Pane;

public class ShapeGroup implements IShape {
    private List<IShape> shapes = new ArrayList<>();

    public void add(IShape shape) {
        shapes.add(shape);
    }

    public void remove(IShape shape) {
        shapes.remove(shape);
    }

    public List<IShape> getShapes() {
        return shapes;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public void setSelected(boolean selected) {

    }

    @Override
    public boolean isOn(double x, double y) {
        return false;
    }

    @Override
    public void offset(double x, double y) {

    }

    @Override
    public void addShapeToPane(Pane pane) {

    }

    @Override
    public void removeShapeFromPane(Pane pane) {

    }

    // ... other methods of the IShape interface
}
