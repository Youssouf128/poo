package drawing.handlers;

import drawing.shapes.IShape;
import drawing.shapes.ShapeAdapter;
import javafx.scene.shape.Polygon;

public class TriangleButtonHandler extends ShapeButtonHandler {

    public TriangleButtonHandler(DrawingPane drawingPane) {
        super(drawingPane);
    }

    @Override
    protected IShape createShape() {
        double x = Math.min(originX, destinationX);
        double y = Math.min(originY, destinationY);
        double width = Math.abs(destinationX - originX);
        double height = Math.abs(destinationY - originY);

        /************** point 1 ****************/
        double X1 = width / 2;
        double Y1 = 0;

        /************** point 2 ****************/
        double X2 = width;
        double Y2 = height;

        /************** point 3 ****************/
        double X3 = 0;
        double Y3 = height;

        Polygon poly = new Polygon(X1, Y1, X2, Y2, X3, Y3);

        poly.setLayoutX(x);
        poly.setLayoutY(y);

        poly.getStyleClass().add("triangle");

        //Ellipse ellipse = new Ellipse(x + width / 2, y + height / 2, width / 2, height / 2);
        //ellipse.getStyleClass().add("ellipse");
        return new ShapeAdapter(poly);
    }

}
