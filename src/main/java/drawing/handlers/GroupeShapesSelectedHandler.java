

package drawing.handlers;

import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.commands.GroupeShapesSelectedCommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import drawing.commands.ICommand;
import drawing.commands.CommandHistory;
import drawing.shapes.IShape;

public class GroupeShapesSelectedHandler implements EventHandler<MouseEvent> {
    private CommandHistory commandHistory;
    private List<IShape> selectedShapes;
    private DrawingPane drawingPane;

    public GroupeShapesSelectedHandler(CommandHistory commandHistory, List<IShape> selectedShapes, DrawingPane drawingPane) {
        this.commandHistory = commandHistory;
        this.selectedShapes = selectedShapes;
        this.drawingPane = drawingPane;
    }

    public void handle(MouseEvent event) {
        if (!selectedShapes.isEmpty()) {
            ShapeGroup shapeGroup = new ShapeGroup();
            ICommand groupCommand = new GroupeShapesSelectedCommand(shapeGroup, selectedShapes, drawingPane);
            commandHistory.exec(groupCommand);
        }
    }
}
