package drawing.handlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RedoButtonHandler implements EventHandler<ActionEvent> {

    DrawingPane drawingPane;

    public RedoButtonHandler(DrawingPane drawingPane){
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent actionEvent) {

        this.drawingPane.getCommandHistory().redo();
    }
}
