package drawing.commands;

import drawing.handlers.DrawingPane;
import drawing.shapes.IShape;

import java.util.ArrayList;

public class AddShapeCmd implements ICommand {
    private DrawingPane drawingPane;
    private IShape shape;

    public AddShapeCmd(DrawingPane drawingPane, IShape shape) {
        this.drawingPane = drawingPane;
        this.shape = shape;
    }

    @Override
    public void execute() {
        drawingPane.addShape(shape);
    }

    @Override
    public void undo() {
        drawingPane.removeShape(shape);
    }
}

