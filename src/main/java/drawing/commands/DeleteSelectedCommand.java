package drawing.commands;

import drawing.handlers.DrawingPane;
import drawing.shapes.IShape;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.shape.Shape;

public class DeleteSelectedCommand implements ICommand {
    private DrawingPane drawingPane;
    private List<IShape> deletedShapes;

    public DeleteSelectedCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        deletedShapes = new ArrayList<>();
    }

    @Override
    public void execute() {
            Iterator iterator = drawingPane.getSelection();
            while (iterator.hasNext()) {
                IShape shape = (IShape) iterator.next();
                deletedShapes.add(shape);
                drawingPane.removeShape(shape);
            }
    }

    @Override
    public void undo() {
        for (IShape shape : deletedShapes) {
            drawingPane.addShape(shape);
        }
    }
}