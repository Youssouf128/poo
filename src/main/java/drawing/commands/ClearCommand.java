package drawing.commands;

import drawing.handlers.DrawingPane;
import drawing.shapes.IShape;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class ClearCommand implements ICommand {
    private DrawingPane drawingPane;
    private List<Node> shapes;

    public ClearCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void execute() {
        shapes = new ArrayList<>(drawingPane.getChildren());
        drawingPane.getChildren().clear();
    }

    @Override
    public void undo() {
        drawingPane.getChildren().addAll(shapes);
    }
}
