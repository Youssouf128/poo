package drawing.commands;

import drawing.handlers.DrawingPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.ArrayList;
import java.util.Stack;

public class CommandHistory {
    private Stack<ICommand> commandStack = new Stack<>();
    private Stack<ICommand> undoStack = new Stack<>();

    public void exec(ICommand command) {
        commandStack.push(command);
        command.execute();
        undoStack.clear();
    }

    public void undo() {
        if (!commandStack.empty()) {
            ICommand command = commandStack.pop();
            command.undo();
            undoStack.push(command);
        }
    }

    public void redo() {
        if (!undoStack.empty()) {
            ICommand command = undoStack.pop();
            commandStack.push(command);
            command.execute();
        }
    }
}