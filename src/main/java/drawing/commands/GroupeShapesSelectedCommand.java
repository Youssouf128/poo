package drawing.commands;
//package drawing.handlers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import drawing.commands.ICommand;
import drawing.handlers.DrawingPane;
import drawing.handlers.ShapeGroup;
import drawing.shapes.IShape;
import javafx.scene.Node;
import javafx.scene.shape.Shape;

import javafx.scene.Group;
import javafx.scene.shape.Shape;
import java.util.List;
import drawing.commands.ICommand;
import drawing.shapes.IShape;

public class GroupeShapesSelectedCommand implements ICommand {
    private ShapeGroup shapeGroup;
    private List<IShape> selectedShapes;
    private DrawingPane drawingPane;

    public GroupeShapesSelectedCommand(ShapeGroup shapeGroup, List<IShape> selectedShapes, DrawingPane drawingPane) {
        this.shapeGroup = shapeGroup;
        this.selectedShapes = selectedShapes;
        this.drawingPane = drawingPane;
    }

    public void execute() {
        for (IShape shape : selectedShapes) {
            shapeGroup.add(shape);
            drawingPane.removeShape(shape);
        }
        drawingPane.addShape(shapeGroup);
    }

    public void undo() {
        drawingPane.removeShape(shapeGroup);
        for (IShape shape : shapeGroup.getShapes()) {
            drawingPane.addShape(shape);
        }
        shapeGroup.getShapes().clear();
    }
}
