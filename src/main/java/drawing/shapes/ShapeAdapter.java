package drawing.shapes;

import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

public class ShapeAdapter implements IShape {

    Shape shape;
    double orgTX;
    double orgTY;

    public ShapeAdapter(Shape shape){

        this.shape = shape;
        this.orgTX = shape.getTranslateX();
        this.orgTY = shape.getTranslateY();
    }
    @Override
    public boolean isSelected() {
        if(shape.getStyleClass().contains("selected")){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void setSelected(boolean selected) {

        if(selected){
            shape.getStyleClass().add("selected");
        }else{
            shape.getStyleClass().remove("selected");
        }

    }

    @Override
    public boolean isOn(double x, double y) {

        if(shape.getBoundsInParent().contains(x, y)){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void offset(double x, double y) {
        if (isSelected()) {
            shape.setTranslateX(this.orgTX + x);
            shape.setTranslateY(this.orgTY + y);
        }
    }

    @Override
    public void addShapeToPane(Pane pane) {
        pane.getChildren().add(this.shape);
    }

    @Override
    public void removeShapeFromPane(Pane pane) {
        pane.getChildren().remove(this.shape);
    }





}
