package drawing.shapes;

import javafx.scene.layout.Pane;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShapeComposite implements IShape{

    ArrayList<IShape> shape_adapter = new ArrayList<>();

    public ArrayList<IShape> getList(){
        return shape_adapter;
    }

    public void addShape(IShape shape){
        shape_adapter.add(shape);
    }
    public void removeShape(IShape shape){
        shape_adapter.remove(shape);
    }

    @Override
    public boolean isSelected() {
        return shape_adapter.get(0).isSelected();
    }

    @Override
    public void setSelected(boolean selected) {

        for (int i=0; i<shape_adapter.size(); i++){
            shape_adapter.get(i).setSelected(selected);
        }
    }

    @Override
    public boolean isOn(double x, double y) {
        int c = 0;
        for (int i=0; i<shape_adapter.size(); i++){
            boolean b = shape_adapter.get(i).isOn(x,y);
            if(b==true){
                c++;
            }
        }

        if (c > 0){
            return true;
        }else{
            return false;
        }


    }

    @Override
    public void offset(double x, double y) {
        for (int i=0; i<shape_adapter.size(); i++){
            shape_adapter.get(i).offset(x, y);
        }
    }

    @Override
    public void addShapeToPane(Pane pane) {

        for (int i=0; i<shape_adapter.size(); i++){
            shape_adapter.get(i).addShapeToPane(pane);
        }
    }



    @Override
    public void removeShapeFromPane(Pane pane) {

        for (int i=0; i<shape_adapter.size(); i++){
            shape_adapter.get(i).removeShapeFromPane(pane);
        }
    }


    public Iterator<IShape> iterator() {
        return shape_adapter.iterator();
    }

}
