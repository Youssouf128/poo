package drawing;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.control.Button;
import java.util.ArrayList;

public class ButtonFactory {

    public int style;
    public static ArrayList<String> list_btn_name;

    public ButtonFactory(int style){
        this.style = style;
        list_btn_name = new ArrayList<String>();
        list_btn_name.add("clearButton");
        list_btn_name.add("Delete");
        list_btn_name.add("Rectangle");
        list_btn_name.add("Triangle");
        list_btn_name.add("Circle");
        list_btn_name.add("Grouper");
        list_btn_name.add("Degrouper");
        list_btn_name.add("Annuler");
        list_btn_name.add("Redo");

    }

    public Button createButton(String buttonName){
            if (list_btn_name.contains(buttonName)){

                    Button btn = new Button();
                if (this.style==0) {
                    btn.setText(buttonName);
                    btn.getStyleClass().add(buttonName);
                }else if(this.style==1) {

                    ImageView view = new ImageView(new Image("file:img/" + buttonName + ".png"));
                    view.setFitHeight(20);
                    view.setPreserveRatio(true);
                    btn.setGraphic(view);
                }
                return btn;
            }else{
                return new Button();
            }

    }
}
