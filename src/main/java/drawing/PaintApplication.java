package drawing;

import drawing.commands.CommandHistory;
import drawing.handlers.DrawingPane;
import drawing.shapes.IShape;
import drawing.ui.StatusBars;
import drawing.ui.ToolBar;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.List;

/**
 * Created by lewandowski on 20/12/2020.
 */
public class PaintApplication extends Application {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private Scene scene;
    private BorderPane root;

    private CommandHistory commandHistory;
    private List<IShape> selectedShapes;
    private DrawingPane drawingPane;



    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new BorderPane();
        scene = new Scene(root, WIDTH, HEIGHT);


        root.getStylesheets().add(
                PaintApplication.class.getClassLoader().getResource("style/Paint.css").toExternalForm());

        drawingPane = new DrawingPane();
        drawingPane.getStyleClass().add("drawingPane"); // on ajoute style sur drwingPane default = white
        root.setCenter(drawingPane); // on place au centre nos drawingPane


        StatusBars bars = new StatusBars(drawingPane);
        root.setBottom(bars);

        HBox hBox = new HBox();
        ToolBar toolBar = new ToolBar(drawingPane);

        hBox.getChildren().addAll(toolBar.getClearButton(), toolBar.getDeleteButton(),toolBar.getRectangleButton(), toolBar.getTriangleButton(),toolBar.getCircleButton(), toolBar.getGroupeButton(), toolBar.getDegroupeButton(), toolBar.getAnnulerButton(), toolBar.getRedoButton());
        hBox.setPadding(new Insets(5));
        hBox.setSpacing(5.0);
        hBox.getStyleClass().add("toolbar");

        root.setTop(hBox);// hBox va contenir nos boutton


        primaryStage.setTitle("Drawing");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public DrawingPane getDrawingPane() {
        return drawingPane;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
