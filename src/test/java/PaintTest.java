import drawing.PaintApplication;
import drawing.shapes.ShapeAdapter;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import static org.junit.Assert.*;

public class PaintTest extends ApplicationTest {

    PaintApplication app;

    @Override
    public void start(Stage stage) {
        try {
            app = new PaintApplication();
            app.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_draw_circle_programmatically() {

        interact(() -> {
                    app.getDrawingPane().addShape(new ShapeAdapter(new Ellipse(20, 20, 30, 30)));
                });

        /*Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Ellipse);
        assertFalse(it.hasNext());
        */

        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().iterator()), 1);
    }

    @Test
    public void should_draw_circle() {
        // given:
        clickOn("Circle");
        moveBy(60,60);

        // when:
        drag().dropBy(70,40);
        // then:
        /*
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Ellipse);
        assertFalse(it.hasNext());
        */
        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().iterator()), 1);
    }

    @Test
    public void should_draw_rectangle() {
        // given:
        clickOn("Rectangle");
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
       /*
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Rectangle);
        assertFalse(it.hasNext());
        */

        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().iterator()), 1);
    }

    @Test
    public void should_draw_triangle() {
        // given:
        clickOn("Triangle");
        moveBy(0,60);

        // when:
        drag().dropBy(70,40);

        // then:
        /*
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof Polygon);
        assertEquals(it.hasNext(), 1);
        */

        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().iterator()), 1);
    }




    @Test
    public void nbr_forms_selected() {
        // given:
        clickOn("Rectangle");
        moveBy(30,60).drag().dropBy(70,40);

        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);

        clickOn("Triangle");
        moveBy(283,74).drag().dropBy(70,40);

        // when:
        drag().dropBy(70,40); // on bouge un peu le triangle

        // Then:
        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().iterator()), 3);
        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().getSelection()), 1);
    }


    @Test
    public void delete_selected_forms() {
        // given:
        clickOn("Rectangle");
        moveBy(30,60).drag().dropBy(70,40);

        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);

        clickOn("Triangle");
        moveBy(283,74).drag().dropBy(70,40);

        // when:
        drag().dropBy(70,40);
        clickOn("Delete");


        // Then:
        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().iterator()), 2); // nbr_form
        assertEquals(app.getDrawingPane().getIteratorSize(app.getDrawingPane().getSelection()), 0); //nbr_forms_selected
    }

    @Test
    public void should_clear() {
        // given:
        clickOn("Rectangle");
        moveBy(30,60).drag().dropBy(70,40);
        clickOn("Circle");
        moveBy(-30,160).drag().dropBy(70,40);
        clickOn("Triangle");
        moveBy(283,74).drag().dropBy(70,40);


        // when:
        clickOn("clearButton");

        // then:
        assertFalse(app.getDrawingPane().iterator().hasNext());
        assertFalse(app.getDrawingPane().getSelection().hasNext());
    }

}